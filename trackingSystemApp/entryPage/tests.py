from django.test import SimpleTestCase, TestCase
from django.urls import reverse, resolve
from . import views
# Url and View Tests
class UrlTest(SimpleTestCase):
    def test_homepage(self):
        response = reverse('index')
        self.assertEquals(resolve(response).func,views.index)


class ViewTest(TestCase):
    def test_homepageview_exists(self):
        response = self.client.get(reverse('index'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'index.html')
var a=$('#d');
var b=$('#d1');
a.find('p').hide();
a.find('li').removeClass('hoverOver');
function listHover(a,b){
    a.find('li').hover(function() {
        $(this).addClass("bg-primary hoverOver");
        $(this).find('p').fadeIn("slow");
        $(this).click(function() {
            $(this).find('#orders').prop("checked", true);
            var val=$(this).find('#orders').val();
            b.find('#order_no').val(val);
        });
    },function() {
        $(this).removeClass("bg-primary hoverOver");
        $(this).find('p').hide();
    });
};

listHover(a,b);
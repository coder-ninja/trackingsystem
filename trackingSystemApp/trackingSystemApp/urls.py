from django.contrib import admin
from django.urls import include,path
from authPage.views import castHouseA,userLogout,castHouseB,operatorRollingPlant,potLineOperator,potLineTransporter,acknowledge,indoorMap
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('entryPage.urls')),
    path('authPage/',include('authPage.urls')),
    path('castHouseA/',castHouseA,name="castHouseA"),
    path('castHouseB/', castHouseB, name="castHouseB"),
    path('operatorRollingPlant/',operatorRollingPlant,name="operatorRollingPlant"),
    path('potLineOperator/',potLineOperator,name='potLineOperator'),
    path('potLineTransporter/',potLineTransporter,name='potLineTransporter'),
    path('acknowledge/',acknowledge,name='acknowledge'),
    path('logout/',userLogout,name="userLogout"),
    path('indoorMap',indoorMap,name="indoorMap"),
]

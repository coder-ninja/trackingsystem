from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from authPage.models import users_role, order,tapping_schedule,transportation_schedule
from .forms import orderForm, tappingScheduleForm, transportationScheduleForm
import datetime

# Create your views here.
def authPage(request): 
    context = {}
    errorMsg = None
    if request.method == "POST":
        email_id = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=email_id, password=password)
        if user:
            login(request, user)
            x = user.users_role
            role = 0
            role = x.users_role
            if role==0:
                logout(request)
                return HttpResponseRedirect(reverse('authPage'))
            if role==1:
                return HttpResponseRedirect(reverse('castHouseA'))
            if role==2:
                return HttpResponseRedirect(reverse('castHouseB'))
            if role==3:
                return HttpResponseRedirect(reverse('operatorRollingPlant'))
            if role==4:
                return HttpResponseRedirect(reverse('potLineOperator'))
            if role==5:
                return HttpResponseRedirect(reverse('potLineTransporter'))           
        else:
            errorMsg = "Provide valid credentials !"
            context["error"] = errorMsg
            return render(request, "login.html", context)
    else:
        return render(request, "login.html", context)


@login_required(login_url="/login/")
def castHouseA(request):
    context = {}
    errorMsg = None
    currentUser = request.user
    currentUserRole = currentUser.users_role.users_role
    context['currentUserRole'] = currentUserRole
    myform = orderForm()
    if request.method == "POST":
        myform = orderForm(request.POST)
        x = None    
        if currentUserRole==1:
            x = "castHouseA"
        elif currentUserRole==2:
            x = "castHouseB"
        elif currentUserRole==3:
            x = "operatorRollingPlant"
        else:
            errorMsg = "User cannot request orders"
            context['error'] = errorMsg
            return render(request, 'castHouseA.html', errorMsg) 

        a = myform.save(commit=False)
        a.requested_user_id=currentUserRole
        a.requested_by=x
        a.is_ack=False
        a.ack=None 
        a.balance=0 
        a.received=0
        a.save()
        errorMsg = "Order Created Successfully!"
        context['error'] = errorMsg
    context['form'] = myform
    return render(request, "castHouseA.html",context)


@login_required(login_url="/login/")
def acknowledge(request):
    context = {}
    errorMsg = None
    currentUser=request.user
    currentUserRole=currentUser.users_role.users_role
    context['currentUserRole'] = currentUserRole
    orders = order.objects.filter(is_ack=False).values(
        'id',
        'ladles_required',
        'requested_by',
        'shift').order_by('id')
    context['orders'] = orders
    if currentUserRole==4 or currentUserRole==5:
        return HttpResponseRedirect(reverse('authPage'))
    if request.method == "POST":
        id = request.POST['order_no']
        ladlesRecieved = request.POST['ladles_received']
        selectedOrder = order.objects.get(id=id)
        y = None    
        if selectedOrder.requested_by=='castHouseA':
            y = 1
        if selectedOrder.requested_by=='castHouseB':
            y = 2
        if selectedOrder.requested_by=='operatorRollingPlant':
            y = 3
        if( y != currentUserRole):
            errorMsg = "Current user cannot acknowledge this order !"
            context['error2'] = errorMsg
            return render(request, 'acknowledge.html', context)
        selectedOrder.balance=int(selectedOrder.ladles_required)-int(selectedOrder.received)
        if(selectedOrder.balance<0):
            errorMsg = "Ladles received greater than ladles required !"
            context['error2'] = errorMsg
            return render(request, 'acknowledge.html', context)
        selectedOrder.ack=currentUser.username
        selectedOrder.is_ack=True
        selectedOrder.received=ladlesRecieved
        selectedOrder.save()
        errorMsg = "Order Number {} Acknowledged Successfully".format(id)
        context['error2'] = errorMsg
        return render(request, 'acknowledge.html', context)

    if orders.count() > 0:
        return render(request, 'acknowledge.html', context)
    else:
        errorMsg = "No orders left to acknowledge !"
        context['error1'] = errorMsg
        return render(request, 'acknowledge.html', context)
        

@login_required(login_url="/login/")
def potLineOperator(request):
    context = {}
    errorMsg = None
    currentUser = request.user
    currentUserRole = currentUser.users_role.users_role
    context['currentUserRole'] = currentUserRole
    if(currentUserRole != 4):
        return HttpResponseRedirect(reverse('authPage'))
    orders=order.objects.filter(schedule=False).values(
        'id',
        'ladles_required',
        'requested_by',
        'shift').order_by('id')
    context['orders'] = orders
    myform = tappingScheduleForm()
    context['form'] = myform
    now = datetime.datetime.now().strftime('%H:%M:%S')
    context['time']=now
    if request.method == "POST":
        id = request.POST['order_no']  
        myform = tappingScheduleForm(request.POST)
        selectedOrder = order.objects.get(id=id)
        a = myform.save(commit=False)
        a.order=selectedOrder
        a.shift=selectedOrder.shift
        a.requested_by=selectedOrder.requested_by
        a.ladles_required=selectedOrder.ladles_required
        a.save() 
        selectedOrder.schedule=True
        selectedOrder.save()
        errorMsg = "Tapping Schedule Generated Successfully !"
        context['error2'] = errorMsg
        return render(request, 'potLineOperator.html', context)
    
    if orders.count() > 0:
        return render(request, 'potLineOperator.html', context)
    else:
        errorMsg = "No orders left !"
        context['error1'] = errorMsg
        return render(request, 'potLineOperator.html', context)
    

@login_required(login_url="/login/")
def potLineTransporter(request):
    context = {}
    errorMsg = None
    currentUser = request.user
    currentUserRole = currentUser.users_role.users_role
    context['currentUserRole'] = currentUserRole
    if(currentUserRole != 5):
        return HttpResponseRedirect(reverse('authPage'))   
    tapped = tapping_schedule.objects.filter(transportation=False).values(
        'id',
        'ladles_required',
        'pots_required',
        'requested_by',
        'time_to_tap',
        'shift').order_by('id')
    context['orders'] = tapped
    myform = transportationScheduleForm()
    context['form'] = myform
    now = datetime.datetime.now().strftime('%H:%M:%S')
    context['time']=now
    if request.method == "POST":
        id = request.POST['order_no']
        myform = transportationScheduleForm(request.POST)
        selectedTapped = tapping_schedule.objects.get(id=id)
        a = myform.save(commit=False)
        a.tapping=selectedTapped
        a.shift=selectedTapped.shift
        a.destination=selectedTapped.requested_by
        a.ladles_required=selectedTapped.ladles_required
        a.pots_required=selectedTapped.pots_required
        a.time_to_tap=selectedTapped.time_to_tap
        selectedTapped.transportation=True
        selectedTapped.save()
        a.save()
        errorMsg = "Transporter Schedule Generated Successfully !"
        context['error2'] = errorMsg
        return render(request, 'potLineTransporter.html', context)
    
    if tapped.count() > 0:
        return render(request, 'potLineTransporter.html', context)
    else:
        errorMsg = "No orders left !"
        context['error1'] = errorMsg
        return render(request, 'potLineTransporter.html', context)


@login_required(login_url="/login/")
def indoorMap(request):
    currentUser = request.user
    currentUserRole = currentUser.users_role.users_role
    if(currentUserRole != 5):
        return HttpResponseRedirect(reverse('authPage'))  
    return render(request, 'map.html')


def userLogout(request): 
    logout(request)
    return HttpResponseRedirect(reverse('authPage'))


@login_required(login_url="/login/")
def castHouseB(request):
    pass


@login_required(login_url="/login/")
def operatorRollingPlant(request):
    pass

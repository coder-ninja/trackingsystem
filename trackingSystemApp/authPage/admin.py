from django.contrib import admin
from .models import users_role,order,tapping_schedule,transportation_schedule
# Register your models here.
admin.site.register(users_role)
admin.site.register(order)
admin.site.register(tapping_schedule)
admin.site.register(transportation_schedule)

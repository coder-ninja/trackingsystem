from .models import order, tapping_schedule, transportation_schedule
from django.forms import ModelForm
# Forms 
class orderForm(ModelForm):
    class Meta:
        model = order
        fields = ['shift','ladles_required']


class tappingScheduleForm(ModelForm):
    class Meta: 
        model = tapping_schedule
        fields = ['time_to_tap','pots_required']


class transportationScheduleForm(ModelForm):
    class Meta:
        model = transportation_schedule
        fields = ['assign_to_trucks','time_to_calltrucks']


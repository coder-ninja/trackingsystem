from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
# Create your models here.
class users_role(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Cast_HouseA = 1
    Cast_HouseB = 2
    Operator_RollingPlant = 3
    Operator_PotLine = 4
    PotLine_Transporter = 5
    users_role_choices = (
        (Cast_HouseA, 'Cast_HouseA'),
        (Cast_HouseB, 'Cast_HouseB'),
        (Operator_RollingPlant, 'Operator_RollingPlant'),
        (Operator_PotLine, 'Operator_PotLine'),
        (PotLine_Transporter, 'PotLine_Transporter')
    )
    users_role = models.IntegerField(choices=users_role_choices, unique=True)


class order(models.Model):
    one = 1
    two = 2
    three = 3
    four = 4
    shifts = (
        (one, '1'),
        (two, '2'),
        (three, '3'),
        (four, '4'),
    )
    shift = models.IntegerField(choices=shifts)
    requested_user = models.ForeignKey(users_role, on_delete=models.CASCADE)
    is_ack = models.BooleanField(default=False, null=True)
    ack = models.CharField(null=True, max_length=200)
    requested_by = models.CharField(null=True, max_length=200)
    ladles_required = models.IntegerField()
    balance = models.IntegerField(null=True)
    received = models.IntegerField(null=True)
    schedule = models.BooleanField(default=False,null=True)


class tapping_schedule(models.Model):
    one = 1
    two = 2
    three = 3
    four = 4
    shifts = (
        (one, '1'),
        (two, '2'),
        (three, '3'),
        (four, '4'),
    )
    order = models.ForeignKey(order,on_delete=models.CASCADE)
    shift = models.IntegerField(choices=shifts)
    ladles_required = models.IntegerField()
    requested_by = models.CharField(null=True, max_length=200)
    time_to_tap=models.TimeField()
    pots_required = models.IntegerField(null=True)
    transportation=models.BooleanField(null=True,default=False)


class transportation_schedule(models.Model):
    one = 1
    two = 2
    three = 3
    four = 4
    shifts = (
        (one, '1'),
        (two, '2'),
        (three, '3'),
        (four, '4'),
    )
    tapping = models.ForeignKey(tapping_schedule, on_delete=models.CASCADE)
    shift = models.IntegerField(choices=shifts)
    ladles_required = models.IntegerField()
    destination = models.CharField(null=True, max_length=200)
    time_to_tap = models.TimeField()
    pots_required = models.IntegerField(null=True)
    assign_to_trucks = models.IntegerField()
    time_to_calltrucks = models.TimeField()



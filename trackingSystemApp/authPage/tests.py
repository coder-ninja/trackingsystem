from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from . import views
# Url, Views and Model Tests
class UrlTest(SimpleTestCase):
    def test_homepage(self):
        response = reverse('authPage')
        self.assertEquals(resolve(response).func,views.authPage)
    
    def test_logoutpage(self):
        response = reverse('userLogout')
        self.assertEquals(resolve(response).func,views.userLogout)
    
    def test_castHouseApage(self):
        response = reverse('castHouseA')
        self.assertEquals(resolve(response).func,views.castHouseA)
        
    def test_acknowledgepage(self):
        response = reverse('acknowledge')
        self.assertEquals(resolve(response).func,views.acknowledge)


class ViewTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='mrspock@gmail.com', password='startreck', email='mrspock@gmail.com', first_name='mrspock', last_name='spock')
         
    def test_homepage_exists(self):
        response = self.client.get(reverse('authPage'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'login.html')

    def test_logoutpage_redirects(self):
        response = self.client.get(reverse('userLogout'))
        self.assertRedirects(response, '/authPage/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)

    def test_castHouseApage_exists(self):
        response = self.client.get(reverse('castHouseA'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'castHouseA.html')

    def test_acknowledgepage_exists(self):
        response = self.client.get(reverse('acknowledge'))
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'acknowledge.html')


    



    